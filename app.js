var app = new Vue({
    el: '#app',
    data: {
        message: 'Ok bye'
    }
})

var app2 = new Vue({
    el: '#app2',
    data: {
        message: 'open web at ' + new Date().toLocaleString()
    }
})

var app4 = new Vue({
    el: '#app-4',
    data: {
        todos: [
            {text: 'javascript'},
            {text: 'vue'},
            {text: 'build have fun'}
        ]
    }
})

var app5 = new Vue({
    el: '#app-5',
    data: {
        message: 'app message 5'
    },
    methods: {
        reverseMessage: function() {
            this.message = this.message.split(' ').reverse().join(' ')
        }
    }
})
var app6 = new Vue({
    el: '#app-6',
    data: {
        message: 'app message 6'
    }
})