var watchExampleVM = new Vue({
    el: '#watch-example',
    data: {
        question: '',
        answer: 'cant answer ask ? '
    },
    watch: {
        question: function(newQuestion, oldQuestion){
            this.answer = 'asking...',
            this.getAnswer()
        }
    },
    methods: {
        getAnswer: _.debounce(
            function () {
                if (this.question.indexOf('?') === -1){
                    this.answer = 'question have to (?)'
                    return
                }
                this.answer = 'thinking...'
                var vm = this
                axios.get('https://yesno.wtf/api')
                    .then(function (response) {
                        vm.answer = _.capitalize(response.data.answer)
                    })
                    .catch(function (err){
                        vm.answer = 'error, cant access api' + err
                    })
            }, 500
        )
    }
})