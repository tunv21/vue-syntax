Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})

var app7 = new Vue({
    el: '#app-7',
    data: {
        groceryList: [
            { id: 0, text: 'Cà pháo' },
            { id: 1, text: 'Mắm tôm' },
            { id: 2, text: 'Miễn ăn được là được' }
        ]
    }
})